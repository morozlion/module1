﻿using System;
using System.Linq;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 b = new Module1();
            b.SwapItems(12,21);
            b.GetMinimumValue(new int[] { 10, 20, 1 });
        }
        public int[] SwapItems(int a, int b)
        {
            return new[] { b, a };

        }
        public int GetMinimumValue(int[] input)
        {
            return input.Min();
        }
    }
}
