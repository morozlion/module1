﻿using NUnit.Framework;

namespace M1
{
    [TestFixture]
    public class Module1Tests
    {
        [Test]
        
        public void Swap()

        {
            Module1 a = new Module1();
            Assert.AreEqual(new[] { 21, 12 }, a.SwapItems(12, 21));
        }
        
    }
}
